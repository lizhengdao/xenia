package space.rocketnine.xenia;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Intent serviceIntent = new Intent(this, XeniaService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(serviceIntent);
        } else {
            startService(serviceIntent);
        }

        Thread openBrowser = new Thread(() -> {
            try {
                Thread.sleep(1000);

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://127.0.0.1:1967"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
        });
        openBrowser.start();
    }

    public static byte[] readFile(Context context, Uri uri) throws IOException {
        ParcelFileDescriptor pdf = context.getContentResolver().openFileDescriptor(uri, "r");

        assert pdf != null;
        assert pdf.getStatSize() <= Integer.MAX_VALUE;
        byte[] data = new byte[(int) pdf.getStatSize()];

        FileDescriptor fd = pdf.getFileDescriptor();
        FileInputStream fileStream = new FileInputStream(fd);
        fileStream.read(data);
        fileStream.close();

        pdf.close();
        return data;
    }
}
