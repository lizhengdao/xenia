package space.rocketnine.xenia;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {
    String LOGTAG = "xenia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(LOGTAG, "started");
    }

    public void openBrowser(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://127.0.0.1:1967"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void manageCertificates(View view) {
        Intent intent = new Intent(getApplicationContext(), CertificatesActivity.class);
        startActivity(intent);
    }

    public void exit(View view) {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        Intent intent = new Intent(MainActivity.this, XeniaService.class);

        getApplication().stopService(intent);
        finishAffinity();

        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
    }
}