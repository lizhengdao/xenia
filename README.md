# Xenia
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

<a href="https://play.google.com/store/apps/details?id=space.rocketnine.xenia"><img width="121" height="36" alt="Google Play" border="0" src="https://rocketnine.space/static/badge_google_36.png"></a>

[Gemini](https://gemini.circumlunar.space) proxy for Android

## Features

- Use your web browser to browse Gemini

## Screenshots

<a href="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/1.png"><img width="108" height="216" alt="Screenshot" border="0" src="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/thumbnails/1.png"></a>
 &nbsp; <a href="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/2.png"><img width="108" height="216" alt="Screenshot" border="0" src="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/thumbnails/2.png"></a>
 &nbsp; <a href="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/3.png"><img width="108" height="216" alt="Screenshot" border="0" src="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/thumbnails/3.png"></a>
 &nbsp; <a href="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/4.png"><img width="108" height="216" alt="Screenshot" border="0" src="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/thumbnails/4.png"></a>
 &nbsp; <a href="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/5.png"><img width="108" height="216" alt="Screenshot" border="0" src="https://gitlab.com/tslocum/xenia/-/raw/master/metadata/en-US/images/phoneScreenshots/thumbnails/5.png"></a>

## Download

Xenia is available on [Google Play](https://play.google.com/store/apps/details?id=space.rocketnine.xenia).

APKs may also be downloaded from [the developer](https://xenia.rocketnine.space/download/?sort=name&order=desc).

Xenia is pending review for addition to [F-Droid](https://f-droid.org).

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/xenia/issues).

## Dependencies

- [gmitohtml](https://gitlab.com/tslocum/gmitohtml)
